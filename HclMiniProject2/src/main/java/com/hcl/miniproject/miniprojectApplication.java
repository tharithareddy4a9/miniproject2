package com.hcl.miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class miniprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(miniprojectApplication.class, args);
	}

}
